package ca.sharcnet.nerve.nssigw;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LincsPredicatesConfig {

    @Bean
    public LincsRoleRoutePredicateFactory lincsRole() {
        return new LincsRoleRoutePredicateFactory();
    }
}
